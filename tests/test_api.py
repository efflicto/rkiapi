# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import unittest
from rkiapi import api


class TestAPI(unittest.TestCase):

    def test_api(self):
        rki = api.RKIAPI()
        self.assertEqual(rki.get_county_details(county="Köln").name, "Köln")
        self.assertEqual(rki.get_county_details(county="df234e1412fdsfsd"), None)
        self.assertEqual(rki.get_county_details(county="Berlin Spandau").name, "Berlin Spandau")


if __name__ == '__main__':
    unittest.main()
