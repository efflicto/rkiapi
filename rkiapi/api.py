# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import datetime
import requests


class CountyStats:

    def __init__(self, name, description, death_rate, cases, deaths, cases_per_100k, cases_per_population, state,
                 last_update, cases_7_per_100k, recovered):
        self.name = name
        self.description = description
        self.death_rate = death_rate
        self.cases = cases
        self.deaths = deaths
        self.cases_per_100k = cases_per_100k
        self.cases_per_population = cases_per_population
        self.state = state
        self.last_update = datetime.datetime.strptime(last_update.replace(' Uhr', ''), '%d.%m.%Y, %H:%M')
        self.cases_7_per_100k = cases_7_per_100k
        self.recovered = recovered


class RKIAPI:
    rki_county_endpoint = 'https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_Landkreisdaten/' \
                          'FeatureServer/0/query?where=GEN%20%3D%20%27CHANGEME%27&outFields=*&returnGeometry=false&' \
                          'outSR=4326&f=json'

    def get_county_details(self, county):
        try:
            req = requests.get(self.rki_county_endpoint.replace('CHANGEME', county))
            if len(req.json()['features']) > 0:
                return CountyStats(
                    name=req.json()['features'][0]['attributes']['GEN'],
                    description=req.json()['features'][0]['attributes']['BEZ'],
                    death_rate=req.json()['features'][0]['attributes']['death_rate'],
                    cases=req.json()['features'][0]['attributes']['cases'],
                    deaths=req.json()['features'][0]['attributes']['deaths'],
                    cases_per_100k=req.json()['features'][0]['attributes']['cases_per_100k'],
                    cases_per_population=req.json()['features'][0]['attributes']['cases_per_population'],
                    state=req.json()['features'][0]['attributes']['BL'],
                    last_update=req.json()['features'][0]['attributes']['last_update'],
                    cases_7_per_100k=req.json()['features'][0]['attributes']['cases7_per_100k'],
                    recovered=req.json()['features'][0]['attributes']['recovered'],
                )
            else:
                return None
        except Exception as e:
            print(e)
            return None


